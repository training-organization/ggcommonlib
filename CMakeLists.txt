set(LIBRARY_NAME ggcommon)

project(${LIBRARY_NAME})

cmake_minimum_required(VERSION 2.8)

set(CMAKE_CXX_STANDARD 11)

set(SOURCE_FILES
        src/include/MathLibrary.h
        src/MathLibrary.cpp
)

add_library(${LIBRARY_NAME} SHARED ${SOURCE_FILES})

target_include_directories(${LIBRARY_NAME} PUBLIC ${CMAKE_BINARY_DIR})
