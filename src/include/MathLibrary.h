#pragma once

#ifdef _WIN32
    #ifdef ggcommon_EXPORTS
        #define GGCOMMON_EXPORT __declspec(dllexport)
    #else
        #define GGCOMMON_EXPORT __declspec(dllimport)
    #endif
#else
    #define GGCOMMON_EXPORT
#endif

#ifndef gg_MATHLIB_H
#define gg_MATHLIB_H

namespace MathLibrary {
    // This class is exported from the MathLibrary.dll
    class GGCOMMON_EXPORT Functions {
    public:
        // Returns a + b
        static double Add(double a, double b);

        // Returns a * b
        static double Multiply(double a, double b);

        // Returns a + (a * b)
        static double AddMultiply(double a, double b);
    };
}

#endif
